FROM golang:1.21.3 as builder

RUN mkdir /app

COPY ./src /app

WORKDIR /app
RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o server . 

FROM scratch

COPY --from=builder /app/server /

EXPOSE 8000
CMD ["/server"]
